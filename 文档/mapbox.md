# Mapbox GL

定义一个容器的宽高，初始化的时候要使用mapboxgl.accessToken和容器

基础版

```vue
<template>
  <div>
    <div ref="mapBoxRef" style="height: 400px; width: 600px"></div>
  </div>
</template>

<script>
// mapboxgl地图
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import MapboxLanguage from "@mapbox/mapbox-gl-language";
export default {
  data() {
    return {
      map: null,
    };
  },
  components: {},
  created() {},
  mounted() {
    this.init();
  },
  methods: {
    init() {
      this.map = new mapboxgl.Map({
        accessToken:
          "pk.eyJ1IjoibW9rMTIzIiwiYSI6ImNrdnowcGI5ejE4YTcydW9sbHpyMnJ1bG0ifQ.T8OFc77vrgvUjVxPPMNwjg",
        container: this.$refs["mapBoxRef"], // container ID
        style: "mapbox://styles/mapbox/streets-v12", // style URL
        center: [116.3972282409668, 39.90960456049752],
        zoom: 18,
        antialias: false,
        attributionControl: false,
      });
      // 添加语言
      this.map.addControl(new MapboxLanguage({ defaultLanguage: "zh-Hans" }));
      // 添加导航控制条
      this.map.addControl(new mapboxgl.NavigationControl(), "top-left");
      //   全屏控件
      this.map.addControl(new mapboxgl.FullscreenControl());
    },
  },
};
</script>
```



[例子](https://docs.mapbox.com/mapbox-gl-js/example/)

[单击时显示弹窗窗口](https://docs.mapbox.com/mapbox-gl-js/example/popup-on-click/)

[悬停时显示弹窗窗口](https://docs.mapbox.com/mapbox-gl-js/example/popup-on-hover/)