# Vue3

Option API:分散式
Composition API:组合式



## ref和reactive

ref包裹的值是RefImpl对象实例(即可定义基本数据类型也可以定义响应式数据，value值其实是Proxy值 )

reactive包裹的值是Proxy，不管数据有多深都可以被改变(只能定义对象类型的响应式数据)

reactive 重新分配一个新的对象会失去响应式(即重新赋值一个对象)，但是`ref`可以直接修改

```javascript
let car = reactive({brand: "奔驰",price: 1234})
changeCar = () => {
	car = {brand: "奥拓", price: 2345} // 这样写页面不更新
	car = reactive({brand: "奥拓", price: 2345}) // 这样写页面不更新
	car = Object.assign({car, {brand: "奥拓", price: 2345})
}
```



## toRef和toRefs

`toRefs`结构对象，`toRef`结构对象中的某个值

从响应式对象身上结构值的时候，结构的值不是响应式，需要用`toRefs`包裹才是响应式对象

```javascript
let person = reactive({
	name: "zs",
	age: 11 
})
let {name, age} = person
=> let {name, age} = toRefs(person)
```



## computed

### 只读

```javascript
let fullName = computed(() => {
    return firstName + lastName
})
```

### 可读可写

```javascript
let fullName = computed({
    get(){
        return firstName + lastName
    },
    set(val){
        // 对应的操作
    }
})
```



## watch

### 监听ref基本数据类型

```javascript
const count = ref(0)
watch(count, (newValue, oldValue) => {
  /* ... */
})
```

### 监听ref复杂数据类型

```javascript
const person = ref({
    name: "zs",
    age:20
})
watch(person, (newValue, oldValue) => {
  /* ... */
}, {deep: true, immediate: true}) // 如果要监听对象属性值发生改变需要使用深度监听; 如果要立即监听值则需要配置immediate
```

### 监听reactive对象

```javascript
const person = reactive({
    name: "zs",
    age:20
})
watch(person, (newValue, oldValue) => { // reactive默认开启深度监视
  /* ... */
}, ) 
```

### 监听属性值

监听`ref`或`reactive`定义的对象类型数据中的某个属性：

1. 若该属性值不是对象类型(即基本类型)，需要写成函数形式；

2. 若该属性值依然是对象类型，建议写成函数形式且需要开启深度监听

   (写成函数的形式不启用深度监听，属性值发生改变的时候不会被监听到，只有对象改变的时候才监听到；)

   (如果不写成函数的形式，直接监听整个对象，属性值发生改变的时候会被监听到，但是整个对象改变的时候监听不到；)

```javascript
const person = reactive({
    name: "zs",
    age:20,
    car: {
        brand: "大众"
    }
})
// 监听name，只有当name值发生改变的时候才会触发函数
watch(()=>person.name, (newValue, oldValue) => { 
  /* ... */
}) 

// 监听car及car的属性值发生变化
watch(()=>person.car, (newValue, oldValue) => { 
  /* ... */
}, {deep: true}) 
```

### 监听多个属性值

```javascript
const person = reactive({
    name: "zs",
    age:20,
    car: {
        brand: "大众"
    }
})
// 监听name，只有当name值发生改变的时候才会触发函数
watch(()=>person.name, (newValue, oldValue) => { 
  /* ... */
}) 

// 监听car及car的属性值发生变化
watch([()=> person.name, ()=>person.car], (newValue, oldValue) => { 
  /* ... */
}, {deep: true}) 
```



## watchEffect

全局监听，不需要指定监听哪个对象或属性值，直接使用即可，且具备立即监听

```javascript
watchEffect(()=>{
    /* ... */
})
```



## ref

用在普通`DOM`标签上，获取的是`DOM`节点；

用在组件标签上，获取的是组件实例对象；

```vue
<template>
	<h2 ref="title">你好 </h2>
	<Person ref="person"/>
</template>
<script setup>
 	let title = ref(); // 名字需要与DOM中的一样
 	console.log(title.value)
    
    let person = ref()
    console.log(person.value)
</script>
```



## defineExpose

父组件中使用子组件的属性值和方法

```javascript
// 子组件中的属性值
let num = ref(0)
let age = ref(10)

defineExpose({num, age})
======================================

// 父组件中使用,需要给子组件绑定ref，然后获取该组件实例
let comp = ref()
console.log(comp)
```



## props

```javascript
// 只接收
defineProps(['list', 'id'])

// 接收 + 设置默认值
withDefaults(defineProps('list'), {
    list: ()=> []
})
// 接收list同时将props保存起来
let props = defineProps(['list', 'id'])
```



## hooks

将相同属性值和方法抽取到一个文件中，在主文件中引入使用

```javascript
const {sum, add} = useSum()
const {list, getList} = useList()
```



## router

### 路由工作模式

`history`模式和`hash`模式

```js
const router = createRouter({
    history: createWebHistory() // history模式
    history: createWebHashHistory() // hash模式
})
```

### 路由传参

```vue
<template>
	/* 方式一 */
	<RouterLink :to="`/new/detail?id=${new.id}&content=${new.content}`">{{new.title}}</RouteLink>

   /* 方式二： path与query搭配 或 name与query搭配 */
	<RouterLink 
        :to="{
             path: '/new/detail',
             query:{
             	id: new.id,
             	content: new.content
             }
          }">
    	{{new.title}}
    </RouteLink>

	/* 方式三： name与params搭配，params不能与path搭配 */
	<RouterLink 
        :to="{
             name: 'detail',
             params:{
             	id: new.id,
             	content: new.content
             }
          }">
    	{{new.title}}
    </RouteLink>
</template>


```



### 路由的Props配置

```js
{
    name: 'detail',
    path: 'detail/:id/:title/:content',
    component: Detail,
    // 将路由收到的所有params参数作为props传给路由组件(query参数不生效)
    props: true 
    => <Detail id="1" title="2" content="3" />
        组件内接收使用 defineProps(['id', 'title', 'content'])
    // 函数式写法： 可以将需要的query或params返回出去
    props(route){
        return route.query // 如果返回params可以直接使用props：true
    }
    // 对象写法，只能写死参数值，不推荐使用
    props：{
        a: 1,
        b: 2
    }
}
```



### 编程式导航

脱离<RouterLink>实现路由跳转

```js
router.push({
    path: '/new/detail',
    query:{
        id: 1,
        content: '222'
    }
})
```



## Pinia

### 在main.js中引入pinia

```js
import { createApp } from 'vue'
import App from './App.vue'
// 引入
import { createPinia } from 'pinia'

const app = createApp(App);
// 创建安装
app.use(createPinia())
app.mount('#app')
```

### 创建pinia的store文件夹

pinia文件放在项目src下的store文件夹下。

注意：在pinia中id必须唯一，可以在devtool工具内部查询到所有store状态值。同时可以以此命名其他store，比如：mian，user，good等等。pinia和vuex有个通用特性就是当刷新页面的时候，修改的状态会丢失，建议持久化状态值。

创建main.js

```javascript
import { defineStore } from 'pinia'

export const useMainStore = defineStore({
    id: 'main',
    state: () => ({
        counter: localStorage.getItem('counter') || 0,
        name: 'Eduardo',
    }),
    getters: {
        doubleCount: (state) => state.counter * 2
    },
    actions: {
        resetCounter(param) {
            this.counter = param
            localStorage.setItem('counter', this.counter)
        },
    },
})
```

注意：这里的this.counter中的this指向state

创建user.js

```javascript
import { defineStore } from 'pinia'

export const useUserStore = defineStore({
    id: 'user',
    state: () => ({
        username: localStorage.getItem('username') || 'ZhangSan',
        age: 0
    }),
    getters: {
        // username: (state) => state.username
    },
    actions: {
        resetName(param) {
            this.username = param
            localStorage.setItem('username', this.username)
        },
    },
})
```

```ts
import { defineStore } from "pinia";

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    name: "超级管理员",
  }),
  getters: {
    nameLength: (state) => state.name.length,
  },
  actions: {
    async insertPost(data: string) {
      // 可以做异步
      // await doAjaxRequest(data);
      this.name = data;
    },
  },
});
```

### vue组件中使用pinia

```vue
<script setup>
import { ref } from 'vue'
import { useMainStore } from '@/stores/main'
import { useUserStore } from "@/stores/user"
import { storeToRefs, mapActions } from 'pinia'

const main = useMainStore()
const { counter, doubleCount } = storeToRefs(main)

const userStore = useUserStore()
const { username } = storeToRefs(userStore)

defineProps({
    msg: String
})

const count = ref(0)

function increase(params) {
    main.resetCounter(22)
}

function changeName(params) {
    // console.log(params.target.value)
    userStore.resetName(params.target.value)
}
</script>

<template>
    <h1>{{ msg }}</h1>
    <p>{{ counter }} {{ doubleCount }}</p>
    <p>用户名：{{username}}</p>
    <input type="text" @input="changeName">
    <button type="button" @click="count++">count is: {{ count }}</button>
    <button type="button" @click="increase">重置counter</button>
</template>
```

### 修改状态

#### 直接修改

(在vuex中不可这样修改，所见及所得)

```js
function changeName(params) {
    userStore.username = params.target.value
}
```

#### 批量修改状态

```js
userStore.$patch( state =>{
    state.age = 30
    state.username = 'Lisi'
})
```

#### action修改

store仓库文件修改

```js
import { defineStore } from "pinia";

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    name: "超级管理员",
  }),
  getters: {
    nameLength: (state) => state.name.length,
  },
  actions: {
    async changeName(data: string) {
      // 可以做异步
      // await doAjaxRequest(data);
      // 此处不能直接使用state的值，需要通过this读取到state中的值
      this.name = data;
    },
  },
});
```

组件中调用action方法

```js
function changeName(params) {
    userStore.changeName(params.target.value)
}
```



### 读取解构的值

如果直接使用toRefs包裹则属性和方法都会变成ref对象

```js
import {storeToRefs} from 'pinia'

const {sum, school, address} = storeToRefs(userStore())
```



### getters

```js
import { defineStore } from "pinia";

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    name: "超级管理员",
  }),
  getters: {
    nameLength: (state) => state.name.length,
  }
});
```

### $subscribe

监听store中所有的变化



## 组件传值

props

emit

mitt

$attrs

$refs和$parent

provide和inject



## shallowRef和shallowReactive

浅层ref和reactive，只能处理第一层响应式，深层次响应式不能处理



## readonly和shallowReadonly

readonly所有数据都不能修改

shallowReadonly只有浅层次的数据不能修改，深层次的数据可以修改



## toRaw和markRaw

markRaw：标记一个对象，使其永远不会变成响应式；

## customRef

track和trigger

创建一个自定义的Ref，并对其依赖项跟踪和更新触发进行逻辑控制。

```js
let initValue = '你好'
let msg = customRef((track, trigger) => {
    return {
        get(){
            track() // 告诉Vue数据msg很重要，要对msg持续关注，一旦msg变化就去更新
            return initValue
        },
        set(value){
            initValue = value
            trigger() // 通知Vue一下数据msg变化了
        }
    }
})
```

