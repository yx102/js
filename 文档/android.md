生成全局变量

Ctrl+Alt+F



[投屏插件](https://github.com/Genymobile/scrcpy)



### 显式Intent

#### 调用setComponent指定

```java
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_act_start);

    findViewById(R.id.jump).setOnClickListener(this);
}

@Override
public void onClick(View view) {
    Intent intent = new Intent();
    ComponentName component = new ComponentName(this, MainActivity.class);
    intent.setComponent(component);
    startActivity(intent);
}
```

#### 调用setClass指定

```java
public void onClick(View view) {
    Intent intent = new Intent();
    intent.setClass(this, MainActivity.class);
    startActivity(intent);
}
```

#### 在Intent的构造函数中

```java
public void onClick(View view) {
    Intent intent = new Intent(this, MainActivity.class);
    startActivity(intent);
}
```

### 隐式Intent

没有明确指定所要跳转的页面，而是通过一些动作字符串来让系统自动匹配。

![image-20230109224228884](https://img-blog.csdnimg.cn/img_convert/e01c200aa504af8841eebf83ebda272d.png)

例子

```java
public void onClick(View view) {
    String phone = "123456";

    Intent intent = new Intent();
    switch (view.getId()){
        case R.id.btn_dial:
            intent.setAction(Intent.ACTION_DIAL);
            Uri uri = Uri.parse("tel:"+phone);
            intent.setData(uri);
            startActivity(intent);
            break;
        case R.id.btn_sms:
            intent.setAction(Intent.ACTION_SENDTO);
            Uri uri2 = Uri.parse("smsto:"+phone);
            intent.setData(uri2);
            startActivity(intent);
            break;
    }
}
```

跳转到自定义的activity

步骤一：在`AndroidManifest.xml`找到该activity，添加`action`和`category`标签，同时设置exported为true，表示允许被其他activity调用。

![image-20230109230403488](https://img-blog.csdnimg.cn/img_convert/34a491cb03e77313f59c9efb27dd8471.png)

步骤二：调用过程和上面一样：

```java
Intent intent = new Intent();
intent.setAction("android.intent.action.activity2");
intent.addCategory(Intent.CATEGORY_DEFAULT);
startActivity(intent);
```



### 向下一个activity传递数据

在代码中发送消息包裹，调用意图对象的`putExtras`方法，即可`存入消息包裹`；

在代码中接收消息包裹，调用意图对象的`getExtras`方法，即可`取出消息包裹`;

在OnCreat方法中注册接收到的回调函数`register.launch(intent);`

是用`registerForActivityResult`方法来接收返回的数据

```java
public class ActSendActivity extends AppCompatActivity {

    private TextView text;
    private TextView response_text;
    private ActivityResultLauncher<Intent> register;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_send);

        text = findViewById(R.id.text);
        response_text = findViewById(R.id.response_text);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActSendActivity.this, ActReceiveActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("req", text.getText().toString());
                intent.putExtras(bundle);
                register.launch(intent); // 注册回调方法
            }
        });


        register = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result != null) {
                Intent intent = result.getData();
                if (intent != null && result.getResultCode() == ActReceiveActivity.RESULT_OK) {
                    Bundle bundle = intent.getExtras();
                    response_text.setText(bundle.getString("res"));
                }
            }
        });
    }
}
```



### 返回数据给上一个页面

通过finish来结束当前的页面

```java
package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Arrays;

public class ActReceiveActivity extends AppCompatActivity {

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_receive);

        TextView text = findViewById(R.id.text);

        Bundle bundle = getIntent().getExtras();

        String context = bundle.getString("req");
        text.setText(context);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                Bundle bundle1 = new Bundle();
                bundle.putString("res", "响应的数据带回上一级");
                intent.putExtras(bundle);
                // 携带意图返回上一个页面，RESULT_OK标识处理成功
                setResult(ActReceiveActivity.RESULT_OK, intent);
                // 结束当前活动页面
                finish();
            }
        });
    }
}
```



### 获取元数据meta的信息

![image-20230509095246207](https://gitee.com/yx102/pic/raw/master/img/image-20230509095246207.png)

```java
// 获取应用包管理器
PackageManager pm = getPackageManager();
try {
	// 从应用包管理器中获取当前的活动信息
  activityInfo = pm.getActivityInfo(getComponentName(), PackageManager.GET_META_DATA);
  // 获取活动附加的元数据信息
  Bundle bundle = activityInfo.metaData;
  get_meta_name.setText(bundle.getString("weather2"));
} catch (PackageManager.NameNotFoundException e) {
    throw new RuntimeException(e);
}
```



## 布局



## 控件

### 导航栏ToolBar

如果需要将文字居中，需要在ToolBar控件中添加TextView控制文字居中

### 弹窗popupWindow



### 按钮Button

#### 公用click方法

```java
private EditText username;
private EditText pwd;

@SuppressLint("MissingInflatedId")
@Override
protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    findViewById(R.id.login).setOnClickListener(this);
    findViewById(R.id.reset).setOnClickListener(this);
}

@Override
public void onClick(View view) {
   // 点击按钮执行的方法
    switch (view.getId()){
    case R.id.jump: // 对应具体按钮的操作方法
        break;
    case R.id.show:
        break;
    }
}
```

### 单个按钮的click方法

```java
findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

    }
});
```



### 文本编辑框

获取值：`input.getText()`

设置值：`input.setText("心情很不错")`

输入框没有背景：`android:background="@null"`；

输入样式：`inputType="textPassword"`

出现下划线：不写android:background或则自定义一个下滑线

监听值的输入`addTextChangedListener`

```java
EditText edit_input = findViewById(R.id.edit_input);
edit_input.addTextChangedListener(this);

// 在afterTextChanged方法中监听输入框的值
public void afterTextChanged(Editable editable) {
    Log.v("Aa", "隐藏"+editable.length())
}
```

通过setTag记录id名称，通过setText设置输入框文字



### 图像视图

#### 项目内图片

需要设置图片路径`src`

改变显示的图片`setImageResource`

```java
image_view.setImageResource(R.drawable.shuixing);
```

改变图片显示比例`scaleType`：fitXY,fitCenter,centerCrop

#### 加载网络图片

[引入加载图片的jar包](https://github.com/bumptech/glide)；

`AndroidManifest.xml`中开启internet

```xml
<uses-permission android:name="android.permission.INTERNET" />
```



![image-20230513145113549](https://gitee.com/yx102/pic/raw/master/image-20230513145113549.png)



### 复选框

通过`setOnCheckedChangeListener`方法来判断是否点击

```java
CheckBox ck = findViewById(R.id.ck);
ck.setOnCheckedChangeListener(this);

public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
  switch (compoundButton.getId()){
      case R.id.checkBoxBtn:
        // 修改checkbox右边的值
        compoundButton.setText(String.format("您%s了这个CheckBox", b ? "勾选" : "未勾选"));
  }
    }
```



### 单选框按钮组

默认选中`android:checked="true"`或通过方法`radio_group.check(R.id.male);`

通过`checkedId`来判断选中的是哪个单选框

```java
public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        if (checkedId == R.id.male){
            Log.v("aa", "male");
        }else if (checkedId == R.id.female){
            Log.v("aa", "female");
        }
    }
```



按钮可以通过`getCheckedRadioButtonId()`方法来判断哪个按钮组的id被选中



### 提醒对话框AlertDialog

setIcon：设置对话框的标题图标。

setTitle：设置对话框的标题文本。

setMessage：设置对话框的内容文本。

setPositiveButton：设置肯定按钮的信息，包括按钮文本和点击监听器。

setNegativeButton：设置否定按钮的信息，包括按钮文本和点击监听器。

setNeutralButton：设置中性按钮的信息，包括按钮文本和点击监听器，该方法比较少用。

```java
public void onClick(View v) {
    // 创建提醒对话框的建造器
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    // 设置对话框的标题文本
    builder.setTitle("尊敬的用户");
    // 设置对话框的内容文本
    builder.setMessage("你真的要卸载我吗？");
    // 设置对话框的肯定按钮文本及其点击监听器
    builder.setPositiveButton("残忍卸载", (dialog, which) -> {
        tv_alert.setText("虽然依依不舍，但是只能离开了");
    });
    // 设置对话框的否定按钮文本及其点击监听器
    builder.setNegativeButton("我再想想", (dialog, which) -> {
        tv_alert.setText("让我再陪你三百六十五个日夜");
    });

    // 根据建造器构建提醒对话框对象
    AlertDialog dialog = builder.create();
    // 显示提醒对话框
    dialog.show();
}
```



可以通过`builder.setView()`方法将整个表单设置到弹窗中

```java
View view = LayoutInflater.from(this).inflate(R.layout.item_goods, null);

AlertDialog.Builder builder = new AlertDialog.Builder(this);
builder.setTitle("尊敬的用户");
builder.setView(view);
```



### 日期弹窗

当 `android:datePickerMode` 属性设置为 `spinner` 时可以选择年月日

当 `android:datePickerMode` 属性设置为 `calendar` 会显示一个日历选择器

| 属性                                   | 说明                                        |
| -------------------------------------- | ------------------------------------------- |
| android:calendarTextColor              | 日历列表的文本的颜色                        |
| android:calendarViewShown              | 是否显示日历视图                            |
| android:datePickerMode                 | 组件外观，可选值: spinner， calendar 默认   |
| android:dayOfWeekBackground            | 顶部星期几的背景颜色                        |
| android:dayOfWeekTextAppearance        | 顶部星期几的文字颜色                        |
| android:endYear                        | 结束年份，比如 2017                         |
| android:firstDayOfWeek                 | 设置日历列表以星期几开头                    |
| android:headerBackground               | 整个头部的背景颜色                          |
| android:headerDayOfMonthTextAppearance | 头部日期字体的颜色                          |
| android:headerMonthTextAppearance      | 头部月份的字体颜色                          |
| android:headerYearTextAppearance       | 头部年的字体颜色                            |
| android:maxDate                        | 最大日期显示在这个日历视图 mm/dd /yyyy 格式 |
| android:minDate                        | 最小日期显示在这个日历视图 mm/dd /yyyy 格式 |
| android:spinnersShown                  | 是否显示spinner                             |
| android:startYear                      | 设置第一年(内容)，比如 1940 年              |
| android:yearListItemTextAppearance     | 列表的文本出现在列表中                      |
| android:yearListSelectorColor          | 年列表选择的颜色                            |

```java
// 获取日历的一个实例，里面包含了当前的年月日
/*Calendar calendar = Calendar.getInstance();
calendar.get(Calendar.YEAR);
calendar.get(Calendar.MONTH);
calendar.get(Calendar.DAY_OF_MONTH);*/
DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int date) {
        Log.v("data", String.format("您选择的日期是%d年%d月%d日", year, month + 1, date));
    }
}, 2090, 5, 11);
// 显示日期对话框
dialog.show();
break;
```



### 时间弹窗

```java
// 获取日历的一个实例，里面包含了当前的时分秒
Calendar calendar2 = Calendar.getInstance();
// 构建一个时间对话框，该对话框已经集成了时间选择器。
TimePickerDialog time = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int min) {
        Log.v("data", String.format("您选择的时间是%d时%d分", hour, min));
    }
}, calendar2.get(Calendar.HOUR_OF_DAY),
        calendar2.get(Calendar.MINUTE), true);// true表示24小时制，false表示12小时制
time.show();
```



### 下拉框

选项要写在layout文件夹中，如果只要显示文字就直接用TextView为根节点，用**ArrayAdapter数组适配器**，如果有图片有文字可以用LinearLayout作为根节点，且需要**simpleAdapter简单适配器**，列表需要用**基本适配器BaseAdapter**

#### 只有文字的下拉框

layout/item_select.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<TextView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="50dp"
    android:gravity="center"
    android:textColor="#0000ff"
    android:textSize="17sp"
    tools:text="火星" />
```

layout/spinner.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".SpinnerDropdownActivity"
    android:orientation="vertical">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="下拉模式的列表框"
        android:textSize="17sp" />
    <Spinner
        android:id="@+id/sp_dialog"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:spinnerMode="dialog" />
</LinearLayout>
```

java

```java
public class SpinnerDropdownActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


   // 定义下拉列表需要显示的文本数
    private String[] statArray = {"水星", "金星", "地球", "火星", "木星", "土星"};
    private Spinner sp_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_dropdown);

        // 初始化下拉模式的列表框
        initSpinnerForDropdown();
    }

    private void initSpinnerForDropdown() {
        // 从布局文件中获取名叫sp_dropdown的下拉框
        sp_dialog = findViewById(R.id.sp_dialog);
        // 声明一个下拉列表的数组适配器
        ArrayAdapter<String> statAdapter = new ArrayAdapter<>(this, R.layout.item_select, statArray); // 这里是用的是选项的文件

        sp_dialog.setPrompt("请选择");
        sp_dialog.setAdapter(statAdapter);
        sp_dialog.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, "您选择的是"+statArray[i], Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
```

#### 图片和文字的下拉框

![image-20230510171433697](https://gitee.com/yx102/pic/raw/master/img/image-20230510171433697.png)

item

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="horizontal"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:tools="http://schemas.android.com/tools"
    >
    <ImageView
        android:id="@+id/iv_icon"
        android:layout_width="0dp"
        android:layout_height="50dp"
        android:layout_weight="1"
        tools:src="@drawable/diqiu"
        />

    <TextView
        android:id="@+id/tv_name"
        android:layout_width="0dp"
        android:layout_height="match_parent"
        android:layout_weight="3"
        android:textSize="17sp"
        android:text="地球"
        />
</LinearLayout>
```

spinner_icon.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".SpinnerIconActivity"
    android:orientation="vertical">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="请选择" />
    <Spinner
        android:id="@+id/sp_icon"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        />
</LinearLayout>
```

java

```java
public class SpinnerIconActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner sp_icon;
    private String[] starArr = {"水星", "金星", "地球", "火星", "木星", "土星"};
    private int[] iconArr = {
            R.drawable.shuixing, R.drawable.jinxing, R.drawable.diqiu,
            R.drawable.huoxing, R.drawable.muxing, R.drawable.tuxing
    };

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_icon);

        initSpinnerIcon();

    }

    private void initSpinnerIcon() {
        sp_icon = findViewById(R.id.sp_icon);
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for(int i = 0; i < iconArr.length; i++){
            Map<String, Object> item = new HashMap<>();
            item.put("icon", iconArr[i]);
            item.put("name", starArr[i]);
            list.add(item);
        }
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, list, R.layout.spinner_icon, new String[]{"icon", "name"}, new int[]{R.id.iv_icon, R.id.tv_name});
        sp_icon.setAdapter(simpleAdapter);
        sp_icon.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, "您选择的是" + i +"张,"+ starArr[i], Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
```



### 列表ListView

长按不处理点击事件，在setOnItemLongClickListener事件中返回true就不会执行点击事件



### 网格视图GridView

numColumns：列数

horizontalSpacing：横向间距

verticalSpacing：纵向间距



### 滚动视图ScrollView

ScrollView：垂直方向布局，只能有一个子元素

HorizontalScrollView：水平方向滚动，只能有一个子元素



### RecyclerView

需要在`build.gradle`文件中引入包才能使用



### 适配器

![img](https://atts.w3cschool.cn/attachments/image/cimg/2015-12-01_565da6379e31e.jpg)

> - **BaseAdapter**：抽象类，实际开发中我们会继承这个类并且重写相关方法，用得最多的一个Adapter！
> - **ArrayAdapter**：支持泛型操作，最简单的一个Adapter，只能展现一行文字~
> - **SimpleAdapter**：同样具有良好扩展性的一个Adapter，可以自定义多种效果！
> - **SimpleCursorAdapter**：用于显示简单文本类型的listView，一般在数据库那里会用到，不过有点过时， 不推荐使用！



修改列表的按压效果`listSelector`



#### 数组适配器

`new ArrayAdapter<>(context, resource, data)`

`context`：上下文

`resource`：需要展示的布局资源组件list_item

`data`：数据

##### 方式一

通过java代码控制

xml文件

```xml
<ListView
  android:id="@+id/listview"
  android:layout_width="match_parent"
  android:layout_height="wrap_content"
  android:divider="@android:color/holo_red_light"
  android:dividerHeight="5dp"/>
```

java文件

```java
ListView listview = findViewById(R.id.listview);
String[] strs = {"水星", "金星", "地球", "火星", "木星", "土星"};
ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, strs);
listview.setAdapter(adapter);
```



##### 方式二

在res/valuse下创建一个数组资源的xml文件：**arrays.xml**：

```
<?xml version="1.0" encoding="utf-8"?>  
<resources>  
    <string-array name="myarray">  
    <item>语文</item>  
    <item>数学</item>  
    <item>英语</item>  
    </string-array>      
</resources>
```

直接在xml文件中是用`entries`属性

```xml
<ListView
  android:layout_width="match_parent"
  android:layout_height="wrap_content"
  android:entries="@array/myarray" />
```



#### 简单适配器

new SimpleAdapter(Context context, List<? extends Map<String,?>>data,int resource,String[] from,int[] to); 

> -  Context context : 表示上下文对象，就是要展示 ListView 的 Activity，或者是通过 getApplicationContext() 得到的上下文对象
> - List<? extends Map<String,?>>data : 用于在列表中显示的数据，传入的数据必须是 List<? extends Map<String,?>>的实现类
>   - 比如 List<Map<String,Object>>
> -  int resource : ListView 中显示的每行子 View 的资源文件，就是位于 layout 文件夹中的 .xml 布局文件
> -  String[] from : 表示 Map<String,Object> 中存放的 Key 键值，因为它要通过键值才能找到对应的 Value，也就是布局要显示的内容
> -  int[] to : 表示要显示出来的 resource 布局文件的 R.id.xx 值，它和 from 中的数据源选项要一一对应

simple_list_item.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".SimpleAdapterActivity"
    android:orientation="horizontal"
    android:padding="10dp">
    <ImageView
        android:id="@+id/img"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_weight="1"
        android:src="@drawable/diqiu"/>

    <LinearLayout
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_weight="3"
        android:orientation="vertical">
        <TextView
            android:id="@+id/name"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:text="张三" />
    </LinearLayout>
</LinearLayout>
```

simple_list.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".SimpleAdapterActivity"
    android:orientation="horizontal"
    android:padding="10dp">
    <ListView
        android:id="@+id/simple_adapter_list"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />
</LinearLayout>
```

simple_list.java

```java
package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SimpleAdapterActivity extends AppCompatActivity {
    private String[] nameArr = {"水星", "金星", "地球", "火星", "木星", "土星"};
    private int[] iconArr = {
            R.drawable.shuixing, R.drawable.jinxing, R.drawable.diqiu,
            R.drawable.huoxing, R.drawable.muxing, R.drawable.tuxing
    };
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_adapter);

        ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for(int i=0; i<nameArr.length; i++){
            HashMap<String, Object> item = new HashMap<String, Object>();
            item.put("icon", iconArr[i]);
            item.put("name", nameArr[i]);
            list.add(item);
        }

        SimpleAdapter adapter = new SimpleAdapter(this, list, R.layout.activity_simple_adapter_item, new String[]{"icon", "name"}, new int[]{R.id.img, R.id.name});
        ListView simple_adapter_list = findViewById(R.id.simple_adapter_list);
        simple_adapter_list.setAdapter(adapter);
    }
}
```



#### 继承抽象类的适配器

##### 没有按钮

新增一个类继承`BaseAdapter`适配器，复用ConvertView

> 因为界面上有N个Item，就会调用N次getView方法

创建Planet构造函数

```java
public class Planet {
    public int image; // 行星图标
    public String name; // 行星名称
    public String desc; // 行星描述

    public Planet(int image, String name, String desc) {
        this.image = image;
        this.name = name;
        this.desc = desc;
    }

    private static int[] iconArray = {R.drawable.shuixing, R.drawable.jinxing, R.drawable.diqiu,
            R.drawable.huoxing, R.drawable.muxing, R.drawable.tuxing};
    private static String[] nameArray = {"水星", "金星", "地球", "火星", "木星", "土星"};
    private static String[] descArray = {
            "水星是太阳系八大行星最内侧也是最小的一颗行星，也是离太阳最近的行星",
            "金星是太阳系八大行星之一，排行第二，距离太阳0.725天文单位",
            "地球是太阳系八大行星之一，排行第三，也是太阳系中直径、质量和密度最大的类地行星，距离太阳1.5亿公里",
            "火星是太阳系八大行星之一，排行第四，属于类地行星，直径约为地球的53%",
            "木星是太阳系八大行星中体积最大、自转最快的行星，排行第五。它的质量为太阳的千分之一，但为太阳系中其它七大行星质量总和的2.5倍",
            "土星为太阳系八大行星之一，排行第六，体积仅次于木星"
    };

    public static List<Planet> getDefaultList() {
        List<Planet> planetList = new ArrayList<Planet>();
        for (int i = 0; i < iconArray.length; i++) {
            planetList.add(new Planet(iconArray[i], nameArray[i], descArray[i]));
        }
        return planetList;
    }
}
```



PlanetBaseAdapter.java：继承BaseAdapter类

```java
public class PlanetBaseAdapter extends BaseAdapter {

    private Context mContext;
    private List<Planet> mPlaneList;

    public PlanetBaseAdapter(Context mContext, List<Planet> mPlaneList) {
        this.mContext = mContext;
        this.mPlaneList = mPlaneList;
    }

    // 获取列表项的个数
    @Override
    public int getCount() {
        return mPlaneList.size();
    }

    @Override
    public Object getItem(int position) {
        return mPlaneList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            // 根据布局文件item_list.xml生成转换视图对象
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list, null);
            holder = new ViewHolder();
            holder.iv_icon = convertView.findViewById(R.id.iv_icon);
            holder.tv_name = convertView.findViewById(R.id.tv_name);
            holder.tv_desc = convertView.findViewById(R.id.tv_desc);
            // 将视图持有者保存到转换视图当中
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        // 给控制设置好数据
        Planet planet = mPlaneList.get(position);
        holder.iv_icon.setImageResource(planet.image);
        holder.tv_name.setText(planet.name);
        holder.tv_desc.setText(planet.desc);

        return convertView;
    }

    public final class ViewHolder {
        public ImageView iv_icon;
        public TextView tv_name;
        public TextView tv_desc;
    }
}
```

使用创建的`ListViewAdapter`适配器

```java
ListView listview = findViewById(R.id.listview);
ListViewAdapter adapter = new ListViewAdapter(this, items);
listview.setAdapter(adapter);
```



##### 有按钮

在继承类中添加按钮的点击事件，且点击行的时候设置`ViewGroup.FOCUS_BLOCK_DESCENDANTS`不让子控件做处理，点击行的代码时使用继承类的文件中绑定列表的点击事件`listview.setOnItemClickListener`

继承的类

```java
public class ListViewWithBtnAdapter extends BaseAdapter {

    private Context mContext;
    private List<Planet> mPlaneList;

    public ListViewWithBtnAdapter(Context mContext, List<Planet> mPlaneList) {
        this.mContext = mContext;
        this.mPlaneList = mPlaneList;
    }

    @Override
    public int getCount() {
        return mPlaneList.size();
    }

    @Override
    public Object getItem(int i) {
        return mPlaneList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.activity_base_adapter_item, null);
            holder = new ViewHolder();
            holder.base_adapter_item = convertView.findViewById(R.id.base_adapter_item);
            holder.icon = convertView.findViewById(R.id.img);
            holder.name = convertView.findViewById(R.id.name);
            holder.btn = convertView.findViewById(R.id.btn);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Planet planet = mPlaneList.get(i);
      // 重点
        holder.base_adapter_item.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        holder.icon.setImageResource(planet.image);
        holder.name.setText(planet.name);
      // 给每行的按钮添加点击事件
        holder.btn.setOnClickListener(view -> {
            Toast.makeText(mContext, "点击的是："+planet.name, Toast.LENGTH_SHORT).show();
        });

        return convertView;
    };

    public class ViewHolder{

        public LinearLayout base_adapter_item;
        public ImageView icon;
        public TextView name;
        public Button btn;
    }
}
```

使用继承类

```java
public class AdapterWithBtnActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private List<Planet> planetList;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter_with_btn);

        // 获得默认的行星列表
        planetList = Planet.getDefaultList();
        ListView listview = findViewById(R.id.base_adapter_list);
        ListViewWithBtnAdapter base_adapter = new ListViewWithBtnAdapter(this, planetList);
        listview.setAdapter(base_adapter);
        listview.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, planetList.get(i).name, Toast.LENGTH_SHORT).show();
    }
}
```



## 通过xml文件修饰View

`shapes`：设置圆角，边框，填充色，渐变色；

`selector`：设置点击，选中点击效果；

`layer-list`：把item按照顺序层叠显示



## bundle

传递字符串

```java
bundle.putString("req33333", "hahahh");
```

数组

```java
// 传递值
bundle.putSerializable("DATA", new String[]{"1","2","3"}) ;
// 获取值
Intent intent = getIntent() ;
String recvData[] = intent.getStringArrayExtra(key);
Log.i("getSerializable", Arrays.toString(recvData));
```



## 数据存储

SharedPreferences

存储

```java
public void save{
  // 如果勾选了“记住密码”，就把手机号码和密码都保存到共享参数中
  if (isRemember) {
  SharedPreferences.Editor editor = mShared.edit(); // 获得编辑器的对象
  editor.putString("phone", et_phone.getText().toString()); // 添加名叫phone的手机号码
  editor.putString("password", et_password.getText().toString()); // 添加名叫password的密码
  editor.commit(); // 提交编辑器中的修改
  }
}
```

获取

在onCreate方法中获取值

```java
// 从share_login.xml获取共享参数对象
mShared = getSharedPreferences("share_login", MODE_PRIVATE);
// 获取共享参数保存的手机号码
String phone = mShared.getString("phone", "");
// 获取共享参数保存的密码
String password = mShared.getString("password", "");
et_phone.setText(phone); // 往手机号码编辑框填写上次保存的手机号
et_password.setText(password); // 往密码编辑框填写上次保存的密码
```





## 基本类型

### 数组

通过add的方式添加数据

```java
// 方式一
int[] arr = {1,2,4, …}
// 方式二
int[] arr;
arr = new int[]{1,2,3, …};
// 多维数组
int[][] arr = {{1,2,3},{4,5,6},{7,8,9}};
```



### 对象HashMap

通过put的方式添加数据

```java
HashMap<Integer, String> Sites = new HashMap<Integer, String>();
// 添加键值对
Sites.put(1, "Google");
Sites.put(2, "Runoob");
```



### 数组对象HashMap

在map的外面再包一层数组

```java
ArrayList<Map<String, Object>> list = new ArrayList<>();
for(int i = 0; i < iconArr.length; i++){
    Map<String, Object> item = new HashMap<>();
    item.put("icon", iconArr[i]);
    item.put("name", starArr[i]);
    list.add(item);
}
```



### JSON

使用Gson将数组转为json格式

```java
ArrayList tempArr = new ArrayList();
System.out.println(new Gson().toJson(tempArr));
```





## 案例

### 添加数据到列表

![image-20230517134553117](https://gitee.com/yx102/pic/raw/master/img/image-20230517134553117.png)

activity_list.xml

```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <TextView
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="姓名" />
        <EditText
            android:id="@+id/name"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"/>
    </LinearLayout>
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">
        <TextView
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="年龄" />
        <EditText
            android:id="@+id/age"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"/>
    </LinearLayout>
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">
        <TextView
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="工作" />
        <EditText
            android:id="@+id/job"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"/>
    </LinearLayout>
    <Button
        android:id="@+id/addRow"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="添加"/>
    <LinearLayout
        android:layout_width="wrap_content"
        android:layout_height="wrap_content">
        <TextView
            android:layout_width="100dp"
            android:layout_height="wrap_content"
            android:text="姓名"/>
        <TextView
            android:layout_width="100dp"
            android:layout_height="wrap_content"
            android:text="年龄"/>
        <TextView
            android:layout_width="100dp"
            android:layout_height="wrap_content"
            android:text="职业"/>
    </LinearLayout>
    <ListView
        android:id="@+id/form_list"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />
</LinearLayout>
```

form_list_item.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="horizontal"
    android:layout_width="match_parent"
    android:layout_height="match_parent">
    <TextView
        android:id="@+id/name"
        android:layout_width="100dp"
        android:layout_height="wrap_content"
        android:text="姓名"/>
    <TextView
        android:id="@+id/age"
        android:layout_width="100dp"
        android:layout_height="wrap_content"
        android:text="年龄"/>
    <TextView
        android:id="@+id/job"
        android:layout_width="100dp"
        android:layout_height="wrap_content"
        android:text="职业"/>
</LinearLayout>
```

ListActivity.java

```java
package com.example.demo2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.demo2.adapter.Myadapter;
import com.example.demo2.entity.Person;

import java.util.ArrayList;
import java.util.HashMap;

public class ListActivity extends AppCompatActivity implements View.OnClickListener {
    private ArrayList<Person> list = new ArrayList<>();
    private EditText name;
    private EditText age;
    private EditText job;
    private SimpleAdapter simpleAdapter;
    private ListView listview;
    private Myadapter adapter;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listview = findViewById(R.id.form_list);
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        job = findViewById(R.id.job);
        findViewById(R.id.addRow).setOnClickListener(this);

        adapter = new Myadapter(this, list);
        listview.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addRow:
                Person person = new Person();

                person.setAge(String.valueOf(age.getText()));
                person.setName(String.valueOf(name.getText()));
                person.setJob(String.valueOf(job.getText()));
                list.add(person);
                Log.e("list2222", String.valueOf(person));
                adapter.notifyDataSetChanged();
                break;
        }
    }
}
```

MyApapter.java

```java
package com.example.demo2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.demo2.R;
import com.example.demo2.entity.Person;

import java.util.ArrayList;

public class Myadapter extends BaseAdapter {
    private ArrayList<Person> list = new ArrayList<>();
    private Context mContext;

    public Myadapter(Context context, ArrayList<Person> data) {
        this.list = data;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        Log.e("item", String.valueOf(list));
        ViewHolder holder;
        if(convertView==null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.form_list_item, null);
            holder = new ViewHolder();
            holder.name = convertView.findViewById(R.id.name);
            holder.age = convertView.findViewById(R.id.age);
            holder.job = convertView.findViewById(R.id.job);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Person info = list.get(i);
        holder.name.setText(info.getName());
        holder.age.setText(info.getAge());
        holder.job.setText(info.getJob());
        Log.e("haha", String.valueOf(info));

        return convertView;
    }

    private class ViewHolder{
        TextView name;
        TextView age;
        TextView job;
    }
}
```

Person.java

```java
package com.example.demo2.entity;

public class Person {
    public String age;
    public String name;
    public String job;

    public void setAge(String age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }
}
```



### 带按钮的添加

![image-20230517144247983](https://gitee.com/yx102/pic/raw/master/img/image-20230517144247983.png)

activity_list2.xml

```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <TextView
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="姓名" />
        <EditText
            android:id="@+id/name"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"/>
    </LinearLayout>
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">
        <TextView
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="年龄" />
        <EditText
            android:id="@+id/age"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"/>
    </LinearLayout>
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">
        <TextView
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="工作" />
        <EditText
            android:id="@+id/job"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"/>
    </LinearLayout>
    <Button
        android:id="@+id/addRow"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="添加"/>
    <LinearLayout
        android:layout_width="wrap_content"
        android:layout_height="wrap_content">
        <TextView
            android:layout_width="80dp"
            android:layout_height="wrap_content"
            android:text="姓名"/>
        <TextView
            android:layout_width="80dp"
            android:layout_height="wrap_content"
            android:text="年龄"/>
        <TextView
            android:layout_width="80dp"
            android:layout_height="wrap_content"
            android:text="职业"/>
        <TextView
            android:id="@+id/opt"
            android:layout_width="180dp"
            android:layout_height="wrap_content"
            android:text="操作"/>
    </LinearLayout>
    <ListView
        android:id="@+id/form_list2"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />
</LinearLayout>
```

form_list_item2.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="horizontal"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:descendantFocusability="blocksDescendants">
    <TextView
        android:id="@+id/name"
        android:layout_width="80dp"
        android:layout_height="wrap_content"
        android:text="姓名"/>
    <TextView
        android:id="@+id/age"
        android:layout_width="80dp"
        android:layout_height="wrap_content"
        android:text="年龄"/>
    <TextView
        android:id="@+id/job"
        android:layout_width="80dp"
        android:layout_height="wrap_content"
        android:text="职业"/>

    <Button
        android:id="@+id/edit_btn"
        android:layout_width="80dp"
        android:layout_height="wrap_content"
        android:text="edit" />
    <Button
        android:id="@+id/del_btn"
        android:layout_width="80dp"
        android:layout_height="wrap_content"
        android:text="del" />
</LinearLayout>
```

List2Activity.java

```java
package com.example.demo2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.demo2.adapter.BtnAdapter;
import com.example.demo2.adapter.Myadapter;
import com.example.demo2.entity.Person;

import java.util.ArrayList;

public class List2Activity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<Person> list = new ArrayList<>();
    private EditText name;
    private EditText age;
    private EditText job;
    private ListView listview;
    private BtnAdapter adapter;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list2);

        listview = findViewById(R.id.form_list2);
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        job = findViewById(R.id.job);
        findViewById(R.id.addRow).setOnClickListener(this);

        adapter = new BtnAdapter(this, list);
        listview.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addRow:
                Person person = new Person();
                person.setAge(String.valueOf(age.getText()));
                person.setName(String.valueOf(name.getText()));
                person.setJob(String.valueOf(job.getText()));
                list.add(person);
            		// 通知适配器更新数据
                adapter.notifyDataSetChanged();
                break;
        }
    }
}
```

BtnAdapter.java

```java
package com.example.demo2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demo2.R;
import com.example.demo2.entity.Person;

import java.util.ArrayList;

public class BtnAdapter extends BaseAdapter {
    private ArrayList<Person> list = new ArrayList<>();
    private Context mContext;

    public BtnAdapter(Context context, ArrayList<Person> data) {
        this.list = data;
        this.mContext = context;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        Log.e("item", String.valueOf(list));
        ViewHolder holder;
        if(convertView==null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.form_list_item2, null);
            holder = new ViewHolder();
            holder.name = convertView.findViewById(R.id.name);
            holder.age = convertView.findViewById(R.id.age);
            holder.job = convertView.findViewById(R.id.job);
            holder.edit = convertView.findViewById(R.id.edit_btn);
            holder.del = convertView.findViewById(R.id.del_btn);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Person info = list.get(i);
        holder.name.setText(info.getName());
        holder.age.setText(info.getAge());
        holder.job.setText(info.getJob());

        holder.edit.setOnClickListener(view -> {
            Toast.makeText(mContext, "点击edit的是："+i, Toast.LENGTH_SHORT).show();
        });

        holder.del.setOnClickListener(view -> {
            Toast.makeText(mContext, "点击del的是："+i, Toast.LENGTH_SHORT).show();
            list.remove(i);
            notifyDataSetChanged();
        });
        Log.e("haha", String.valueOf(info));

        return convertView;
    }

    private class ViewHolder{
        TextView name;
        TextView age;
        TextView job;
        Button edit;
        Button del;
    }
}
```

Person.java

```java
package com.example.demo2.entity;

public class Person {
    public String age;
    public String name;
    public String job;

    public void setAge(String age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }
}
```



> 一次性添加多条数据记得在for循环中创建hashmap，在for循环外创建hashmap则每条数据的值都是一样的。



### 异步接口同步执行

通过使用handle的方式来实现

```java
private Handler handler
   
// 子线程
public void getData(){
    // 请求接口得到数据
    Message message = new Message();
    Bundle budle = new Bundle();
  	budle.putString("sdays","传递得到的数据到主线程");
    message.setData(budle);
    handler.sendMessage(message);
}
    

// 主线程
public void add(){
    // 执行子线程的内容
    getData();
    
    handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            //todo 这里做一些事情。。。
            Bundle data = msg.getData();
            System.out.println(data.getString("sdays"))
            super.handleMessage(msg);
        }
    };
}

```



### 批量删除&全选全不选

![image-20231101152406935](https://gitee.com/yx102/pic/raw/master/img/image-20231101152406935.png)

用map初始化所有条数设置为false，用数组记录选中的值

```java
private HashMap<Integer, Boolean> checkMap = new HashMap<>(); // 选中的集合
private List<String> checkedList = new ArrayList<>(); // 选中的数据

// 遍历列表的数据，给每一行都加一个标识
public void initCheckbox(boolean isChecked, ArrayList<HashMap<String, String>> list){
  for(int i = 0; i < list.size(); i++){
    checkMap.put(i, isChecked);
  }
  checkedList.clear();
}

public void addListener(){
  // 批量删除
  batchDelBtn.setOnClickListener(v -> {
    if(checkedList.size() == 0){
      Toast.makeText(this, "请至少选择一条记录", Toast.LENGTH_SHORT).show();
      return;
    }else{
      new AlertDialog.Builder(Death_PigletAddActivity.this).setTitle("删除提示")
        .setMessage("是否要删除选中的记录？")
        .setPositiveButton("确定", (dialog, i1) -> {
          // 删除不能通过for循环删除，数据会出现错乱
            Iterator<HashMap<String, String>> iterator = pigletList.iterator();
          	while(iterator.hasNext()){
              HashMap<String, String> value = iterator.next();
              if(checkedList.contains(value.get("CHECKTAG"))){
                iterator.remove();
              }
            }
          selectAll.setChecked(false);
          initCheckBox(false, pigletList);
          boarAdapter.notifyDataSetchanged();
        }).setNegativeButton("取消", (dialog, i12) -> {
            dialog.cancel();
        }).create().show();
    }
  });
  // 全选或全不选（不要用checkedchange监听，当行取消选中，全选重新设置值会触发）
  // TODO
  batchDelBtn.setOnClickListener(v -> {
    if(batchDelBtn.isChecked()){
      batchDelBtn.setChecked(false);
      initCheckBox(false, pigletList);
      checkedList.clear();
      // 执行列表更新的方法 notifyDataSetChanged
    }else{
      batchDelBtn.setChecked(true);
      initCheckBox(true, pigletList);
      // 遍历判断是否已经有数据了，如果没有就添加，有就不添加
      for(int i = 0; i < pigletList.size(); i++){
        // 有数据
        if(checkedList.contains(pigletList.get(i).get("CHECKTAG"))){}else{
          checkedList.add(pigletList.get(i).get("CHECKTAG")));
        }
      }
      // 执行列表更新的方法 notifyDataSetChanged
    }
  })
}
```



### 添加行后可以直接修改值

![image-20231024171604542](https://gitee.com/yx102/pic/raw/master/img/image-20231024171604542.png)

使用simpleAdapter

```java
private SimpleAdapter boarAdapter;
private ListView listView;
private final String[] from = {"FSEX", "BOARRFID", "BREED"}; // 需要渲染的属性值
private final String[] to = {R.id.death_pigletau_id, R.id.death_pigletau_boarrfid, R.id.death_pigletau_breed, R.id.death_pigletau_del}; // 通过id找到对应的控件
 
public void onCreate(Bundle savedInstanceState) {
  // 在create方法中初始化就可
  boarAdapter = new death_piglet_EntryAdapter(this, pigletList, R.layout.death_pigletau_item, from, to, edDeathQty);
  listView.setAdapter(boarAdapter);
}

// 继承SimpleAdapter后重写适配器的内容
public class death_piglet_EntryAdapter extends SimpleAdapter {
  private final List<HashMap<String, String>> list;
  private final EditText deathQty;
  
  public death_piglet_EntryAdapter(Context context, List<HashMap<String, String>> data, int resource, String from[], int to[], EditText deathQty){
    super(context, data, resource, from, to);
    this.list = list;
    this.deathQty = deathQty;
  }
  
  @Override
	public void notifyDataSetchanged(){
    deathQty.setText(string.value0f(this.list.size())); //传入的值自动根据列表的行数自动更新
    super.notifyDataSetChanged() ;
  }
  
  @Override
	public View getView(final int position, View convertView, ViewGroup parent) {
    // 这个地方就是决定convertview是否重用的，一定要判断!
    final ViewHolder viewHolder;
    if(viewHolder == null){
      LayoutInflater inflater = Death_PigletAddActivity.this.getLayoutInflater();
      convertView = inflater.inflate(R.layout.death_pigletau_item, root: null); // 找到需要渲染列表的页面
      viewHolder = new ViewHolder();
      viewHolder.tvId = convertView.findViewById(R.id.death_pigletau_item_id);
      viewHolder.etBoarGear = convertView.findViewById(R.id.death_pigletau_item_scfcanboarid);
      viewHolder.tvBreed = convertView.findViewById(R.id.death_pigletau_item_breed);
      viewHolder.del = convertView.findViewById(R.id.death_pigletau_item_del);
      
      viewHolder.etBoarGear.setTag(position);
      
      // 添加touch事件
      class onTouchListener implements View.OnTouchListener{
            private final ViewHolder mHolder;
            public onTouchListener(ViewHolder viewHolder) {
                mHolder = viewHolder;
            }

            @Override
            public boolean onTouch(View view, MotionEvent event) {
              // 弹起时需要做的事情
                if(event.getAction() == MotionEvent.ACTION_UP){
                    final int position = (Integer) mHolder.etBoarRfid.getTag();
                    Intent intent = new Intent(Death_PigletAddActivity.this, BoarSelDialog.class);
                    intent.putExtra("prop",  "0");
                    startActivityForResult(intent,position);
                }
                return false;
            }
      }
      viewHolder.etBoarGear.setOnTouchListener(new onTouchListener(viewHolder));
      
      // 添加点击事件
      class delKey implements View.OnClickListener{
            private final ViewHolder mHolder;
            public delKey(ViewHolder viewHolder) {
                mHolder = viewHolder;
            }

            @Override
            public void onClick(View view) {
                // 点击实现删除的逻辑
                new AlertDialog.Builder(Death_PigletAddActivity.this).setTitle("删除提示").setMessage("确定删除选中记录吗？").setPositiveButton("确定", (dialog, i1) -> {
                    int position = (Integer) mHolder.etBoarRfid.getTag();
                    list.remove(position);
                    boarAdapter.notifyDataSetChanged(); // 删除完后刷新列表数据
                }).setNegativeButton("取消", (dialog, i12) -> {
                    dialog.cancel();
                }).create().show();
            }
        }
        viewHolder.del.setOnClickListener(new delKey(viewHolder));
      
      convertView.setTag(viewHolder);
    }else{
      viewHolder = (viewHolder)  convertView.getTag();
      viewHolder.etBoarRfid.setTag(position);
    }
    
    String value = list.get(position).get(from[0]);
    if(value != null){
      viewHolder.tvId.setText(value);
    }
    value = list.get(position).get(from[1]);
    if(value != null){
      viewHolder.etBoarRfid.setText(value);
    }
    
    return convertView;
  }
  
  private class ViewHolder{
        TextView tvId;
        EditText etBoarRfid;
        EditText evBreed;
    		ImageView del; // 删除按钮是用图片代替(即在xml中是使用ImageView标签)
   }
}
```



### 弹窗不通过校验不允许关闭

```java
View editView = LayoutInflater.from(this).inflate(R.layout.dialog_layout,null);
// 两种方法都可以
//  LayoutInflater inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//  View editView = inflater.inflate(R.layout.dialog_layout,null);
AlertDialog.Builder builder = new AlertDialog.Builder(this);
AlertDialog alert = builder.setTitle("修改")
        .setView(editView)
        .setPositiveButton("确定", null) // 设置为null后下面要设置setOnClickListener才生效
        .setNegativeButton("取消", (dialog, i) -> {
            dialog.dismiss();
        })
        .setCancelable(false)
        .show();

alert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view -> {
  	// 不通过校验无法到达下方的关闭  
  	if(Tools.isEmpty(num)){
        Toast.makeText(this, "数量不能为空", Toast.LENGTH_SHORT).show();
        return;
    }

    alert.dismiss();
});
```



### 扫码添加多个内容

![image-20231024171604542](https://gitee.com/yx102/pic/raw/master/img/image-20231024171604542.png)

点击打开扫码枪，录入多个数据

```java
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_act_send);

    findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(this, CameraActivity.class);
            startActivityForResult(intent, 9999);
        }
    })
}

protected void onActivityResult(int requestCode, int resultCode, Intent data){
  if(resultCode == 9999){
    try{
      String barcode = data.getStringExtra("SCAN_RESULT");
      searchPiglet(); // 查询内容添加到列表中
      Intent intent = new Intent(this, CameraActivity.class);
      startActivityForResult(intent, 9999);
    }catch(Exception e){
      e.printStackTrace();
    }
  }
}
```

CameraActivity

```java

```



### 使用内置数据库SQLite

![image-20231120173838687](https://gitee.com/yx102/pic/raw/master/img/image-20231120173838687.png)

从android导出数据库文件后放到数据库中查看添加的表和新增字段

![image-20231120174118736](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20231120174118736.png)

创建DBHelper

> 更新数据库的时候要修改数据库版本对应的方法才会执行

```java
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "pig.db";
    public static final int VERSION = 1;

    /**
     *
     * @param context 上下文
     * @param name 数据库名称
     * @param factory 游标工厂
     * @param version 版本号
     */
    public DBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    /**
     * 第一次创建数据库时被调用
     * @param sqLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("创建数据库");
        String sql = "create table employee(_id integer, name varchar, age integer)";
        db.execSQL(sql);
    }

    /**
     * 升级数据库时的回调
     * @param sqLiteDatabase
     * @param i
     * @param i1
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        System.out.println("更新数据库");
        String sql = "alter table employee add phone integer"; // 在原表上增加一列
        db.execSQL(sql);
    }
}

```

创建Dao(增删改查)

```java
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Dao {
    private  final  DBHelper mHelper;
    public Dao(Context context){
        mHelper = new DBHelper((context));
    }

    public  void insert(){
        SQLiteDatabase db = mHelper.getWritableDatabase();
        String sql = "insert into employee (_id, name, age) values (?, ?, ?)";
        db.execSQL(sql, new Object[]{1, "zs", 12});
        db.close();
    }

    public void update(){

    }
    public void del(){

    }
    public void query(){

    }
}
```

使用test测试类验证

放在Test文件夹中，直接左侧绿色三角形就会运行

```java
import android.content.Context;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)

public class DataBaseTest {

    @Test
    public void testCreate() {
        // 这里创建数据库

    }

    @Test
    public void testInsert() {
        // 测试插入数据
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Dao dao = new Dao(appContext);
        dao.insert();
    }

    @Test
    public void testDelete() {
        // 测试删除数据
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Dao dao = new Dao(appContext);
//        dao.delete();
    }
}
```



